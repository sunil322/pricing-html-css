document.getElementById("checkbox").addEventListener('change', () => {
    if (document.getElementById('checkbox').checked === true) {
        document.getElementById("basic-plan-price").innerHTML = "199.99";
        document.getElementById("professional-plan-price").innerHTML="249.99";
        document.getElementById("master-plan-price").innerHTML="399.99";
    } else {
        document.getElementById("basic-plan-price").innerHTML = "19.99";
        document.getElementById("professional-plan-price").innerHTML="24.99";
        document.getElementById("master-plan-price").innerHTML="39.99";
    }
});
function resetToggle(){
    document.getElementById('checkbox').checked=false;
}
resetToggle();